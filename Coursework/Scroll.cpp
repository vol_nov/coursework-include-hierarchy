#include "Scroll.h"

ScrollInfo::ScrollInfo() {
	scrollCounterX = 0;
	scrollCounterY = 0;

	scrollDx = 50;
	scrollDy = 50;
}

void ScrollInfo::InitScrollInfo(HWND hWnd, int maxWidth, 
	int maxHeight, RECT rectClient) {
	
	this->scrollDx = (maxWidth) / 100;
	this->scrollDy = (maxHeight) / 100;

	SCROLLINFO info;
	GetScrollInfo(hWnd, SB_VERT, &info);
	info.nMin = 0;
	info.nMax = 100;
	SetScrollInfo(hWnd, SB_VERT, &info, true);

	GetScrollInfo(hWnd, SB_HORZ, &info);
	info.nMin = 0;
	info.nMax = 100;
	SetScrollInfo(hWnd, SB_HORZ, &info, true);
}


void OnHScroll(HWND hWnd, WPARAM wParam, LPARAM lParam, ScrollInfo* scrollInfo, int maxWidth) {

	switch (LOWORD(wParam))
	{
	case SB_LEFT:
		scrollInfo->scrollCounterX = 0;
		break;

	case SB_LINELEFT:
		if (scrollInfo->scrollCounterX < maxWidth)
			scrollInfo->scrollCounterX += scrollInfo->scrollDx;
		break;

	case SB_THUMBPOSITION:

		scrollInfo->scrollCounterX = HIWORD(wParam) * maxWidth  / -100;
		break;

	case SB_THUMBTRACK:
		scrollInfo->scrollCounterX = HIWORD(wParam) * maxWidth / -100;
		break;

	case SB_LINERIGHT:
		if (scrollInfo->scrollCounterX > 0)
			scrollInfo->scrollCounterX -= scrollInfo->scrollDx;
		break;

	case SB_RIGHT:
		scrollInfo->scrollCounterX = maxWidth;
		break;

	case SB_ENDSCROLL:
		break;
	}

	SetScrollPos(hWnd, SB_HORZ, scrollInfo->scrollCounterX * -100 / maxWidth, TRUE);
	InvalidateRect(hWnd, NULL, TRUE);
	UpdateWindow(hWnd);
}
void OnVScroll(HWND hWnd, WPARAM wParam, LPARAM lParam, ScrollInfo* scrollInfo, int maxHeight) {
	switch (LOWORD(wParam))
	{
	case SB_TOP:
		scrollInfo->scrollCounterY = 0;
		break;

	case SB_LINEUP:
		if (scrollInfo->scrollCounterY < maxHeight)
			scrollInfo->scrollCounterY += scrollInfo->scrollDy;
		break;

	case SB_THUMBPOSITION:
		scrollInfo->scrollCounterY = HIWORD(wParam)* maxHeight / -100;
		break;

	case SB_THUMBTRACK:
		scrollInfo->scrollCounterY = HIWORD(wParam)* maxHeight / -100;
		break;

	case SB_LINEDOWN:
		if (scrollInfo->scrollCounterY > 1)
			scrollInfo->scrollCounterY -= scrollInfo->scrollDy;
		break;

	case SB_BOTTOM:
		scrollInfo->scrollCounterY = 100;
		break;

	case SB_ENDSCROLL:
		break;
	}

	SetScrollPos(hWnd, SB_VERT, scrollInfo->scrollCounterY * -100 / maxHeight, TRUE);
	InvalidateRect(hWnd, NULL, TRUE);
	UpdateWindow(hWnd);
}