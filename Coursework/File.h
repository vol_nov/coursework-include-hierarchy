#pragma once

#include <Windows.h>
#include <vector>

class File {
	char* filename;
	std::vector<File*> dependencies;
public:
	File();
	File(char* filename);
	File(char* filename, std::vector<File*> dependencies);

	void AddDependency(File* file);

	char* GetFilename();
	std::vector<File*>& GetDependencies();

	bool ContainsDependency(File* file);
	
	~File();
};
