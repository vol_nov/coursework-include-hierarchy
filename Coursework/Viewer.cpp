#include "Viewer.h"
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

Viewer::Viewer(int maxFilesInLine, int hierarchySize) {
	startX = 20;
	startY = 20;
	horizontalMargin = 30;
	verticalMargin = 50;
	spaceBetweenFilesHor = 70;
	spaceBetweenFilesVer = 100;

	outCrossLineDx = 30;

	maxWidth = (maxFilesInLine + 1) * size.cx + spaceBetweenFilesHor * (hierarchySize + 1);
	maxHeight = 2500;

	hFont = CreateFont(20, 8, 0, 0, FW_MEDIUM, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, TEXT("Calibri"));


	bool exists = ReadColorsFromFile("settings.txt");
	if (!exists) {
		hFileHeaderColor = RGB(28, 232, 255);
		hFileContentColor = RGB(220, 220, 220);
		cppFileHeaderColor = RGB(33, 255, 169);
		cppFileContentColor = RGB(243, 243, 243);
		rcFileColor = RGB(0, 255, 255);
	}
}

void Viewer::OnPaint(HWND hWnd, Hierarchy* hierarchy, ScrollInfo* scrollInfo, RECT rectClient) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	if (hierarchy != nullptr) {
		outCrossLineDx = 30;
		outCrossLineDy = 30;

		startY = scrollInfo->scrollCounterY / 2 + 20;

		SetMaxFileStrSize(hdc, hierarchy);

		GetTextExtentPoint32A(hdc, include, strlenInc, &incSize);

		maxWidth = (hierarchy->GetMaxFilesInLine()) * (size.cx) + spaceBetweenFilesHor * (hierarchy->GetMaxFilesInLine() + 2);

		SelectObject(hdc, hFont);

		LPRECT rect = new RECT();
		int sizeOfHierarchy = hierarchy->GetHierarchy().size();
		std::vector<std::vector<RECT>> drawInfo;

		int maxStartX = 0;

		for (int i = sizeOfHierarchy - 1; i >= 0; i--) {
			int sizeOfFiles = (hierarchy->GetHierarchy()[i]).size();

			int spaces = ((sizeOfFiles > 1) ? (sizeOfFiles - 1) * spaceBetweenFilesHor : size.cx / -2);

			startX = (maxWidth - (sizeOfFiles - 1) * size.cx - spaces + scrollInfo->scrollCounterX) / 2;

			if (maxStartX < startX)
				maxStartX = startX;

			auto modules = (hierarchy->GetHierarchy()[i]);

		    maxRectHeight = spaceBetweenFilesVer;
			int tempDiff1, tempDiff2;

			drawInfo.push_back(std::vector<RECT>());
			for (auto it2 = modules.begin(); it2 != modules.end(); it2++,
				startX += size.cx + horizontalMargin + spaceBetweenFilesHor) {

				auto deps = (*it2)->GetDependencies();


				int prevStartY = startY;
				RECT rectCl = DrawFile(hdc, rect, sizeOfHierarchy, drawInfo, (*it2), i);

				startY += rectCl.bottom - rectCl.top;

				(*it2)->SetCoords(rectCl);

				if (maxRectHeight < startY - prevStartY)
					maxRectHeight = startY - prevStartY;
				startY = prevStartY;
			}
			startY += maxRectHeight + size.cy + spaceBetweenFilesVer;
		}

		for (int i = sizeOfHierarchy - 1; i >= 0; i--) {
			auto modules = hierarchy->GetHierarchy()[i];

			for (auto it = modules.begin(); it != modules.end(); it++) {
				auto deps = (*it)->GetModuleDependencies();

				std::vector<Module*>::iterator it_unique;
				it_unique = std::unique(deps.begin(), deps.end());
				deps.resize(std::distance(deps.begin(), it_unique));

				OrderVectorByLeftCoords(deps);
				
				int spacing = 15;
				int endXdX = -1 * (deps.size() / 2 * spacing);
				int drawDy = spacing * deps.size() / 2;
				for (auto dep = deps.begin(); dep != deps.end(); dep++,
					endXdX += spacing) {
					RECT coords;

					InitConnectionLineInfo(&coords, (*it), (*dep), endXdX);

					std::vector<POINT> pArr;
					if ((*dep)->GetHierarchyLevel() == (*it)->GetHierarchyLevel() - 1) {
						SetOrdinaryConnectionCoords(pArr, coords, drawDy);
					}
					else {
						SetCrossConnectionCoords(pArr, coords, scrollInfo, *dep);
					}
					Polyline(hdc, pArr.data(), pArr.size());

					drawDy -= spacing;

					DrawArrow(hdc, coords.right, coords.bottom);
				}
			}
		}

		maxHeight = startY + size.cy + verticalMargin * hierarchy->GetHierarchy().size() * 10 + spaceBetweenFilesVer ;
		maxWidth = (maxStartX + (hierarchy->GetMaxFilesInLine() + 1)*(size.cx + horizontalMargin + spaceBetweenFilesHor));

		scrollInfo->InitScrollInfo(hWnd, maxWidth, maxHeight, rectClient);

		delete rect;
	}
	EndPaint(hWnd, &ps);
}

void Viewer::SetMaxWidthAndHeight(int maxFilesInLine, int hierarchySize) {
	maxWidth = maxFilesInLine * size.cx + spaceBetweenFilesHor * (hierarchySize - 1);
	maxHeight = 5000;
}

int Viewer::GetMaxWidth() {
	return maxWidth;
}
int Viewer::GetMaxHeight() {
	return maxHeight;
}

void Viewer::SetMaxFileStrSize(HDC hdc, Hierarchy* hierarchy) {
	std::string maxStr = " #include ";
	for (int i = 0; i < hierarchy->GetMaxFilenameLength(); i++) {
		maxStr += "a";
	}

	GetTextExtentPoint32A(hdc, maxStr.c_str(), maxStr.length(), &size);
}


void Viewer::DrawArrow(HDC hdc, int endX, int endY) {
	POINT* pTriangle = new POINT[3];

	pTriangle[0].x = endX;
	pTriangle[0].y = endY;
	pTriangle[1].x = endX - 5;
	pTriangle[1].y = endY + 10;
	pTriangle[2].x = endX + 5;
	pTriangle[2].y = endY + 10;

	Polygon(hdc, pTriangle, 3);

	delete pTriangle;
}


void Viewer::SetOrdinaryConnectionCoords(std::vector<POINT>& pArr, RECT coords, int drawDy) {
	pArr.push_back(POINT());

	pArr[0].x = coords.left;
	pArr[0].y = coords.top;

	pArr.push_back(POINT());

	pArr[1].x = coords.left;
	pArr[1].y = coords.top + (coords.bottom - coords.top) / 2 + abs(drawDy);

	pArr.push_back(POINT());

	pArr[2].x = coords.right;
	pArr[2].y = coords.top + (coords.bottom - coords.top) / 2 + abs(drawDy);

	pArr.push_back(POINT());

	pArr[3].x = coords.right;
	pArr[3].y = coords.bottom;
}
void Viewer::SetCrossConnectionCoords(std::vector<POINT>& pArr, RECT coords, ScrollInfo* scrollInfo, Module* dep) {
	coords.left = dep->GetCoords().left + ((dep)->GetCoords().right - dep->GetCoords().left) / 2;
	coords.top = dep->GetCoords().bottom + 1;

	pArr.push_back(POINT());

	pArr[0].x = coords.left;
	pArr[0].y = coords.top;

	pArr.push_back(POINT());

	pArr[1].x = coords.left;
	pArr[1].y = coords.top + outCrossLineDy;

	pArr.push_back(POINT());

	pArr[2].x = outCrossLineDx + scrollInfo->scrollCounterX;
	pArr[2].y = coords.top + outCrossLineDy;

	pArr.push_back(POINT());

	pArr[3].x = outCrossLineDx + scrollInfo->scrollCounterX;
	pArr[3].y = coords.bottom + outCrossLineDy;

	pArr.push_back(POINT());

	pArr[4].x = coords.right;
	pArr[4].y = coords.bottom + outCrossLineDy;

	pArr.push_back(POINT());

	pArr[5].x = coords.right;
	pArr[5].y = coords.bottom;

	outCrossLineDx += 10;
	outCrossLineDy += 10;
}


void Viewer::InitConnectionLineInfo(RECT* coords, Module* module, Module* dep, int endXdX) {
	coords->left = dep->GetCoords().left + (dep->GetCoords().right - dep->GetCoords().left) / 2;
	coords->top = dep->GetCoords().top - 1;

	coords->right = module->GetCoords().left + (module->GetCoords().right - module->GetCoords().left) / 2 + endXdX;
	coords->bottom = module->GetCoords().bottom - 1;
}

void Viewer::OrderVectorByLeftCoords(std::vector<Module*>& deps) {
	for (int k = 0; k < deps.size(); k++) {
		for (int j = k + 1; j < deps.size(); j++) {
			if (deps[k]->GetCoords().left > deps[j]->GetCoords().left) {
				std::iter_swap(deps.begin() + k, deps.begin() + j);
			}
		}
	}
}

RECT Viewer::DrawFile(HDC hdc, LPRECT rect, int sizeOfHierarchy, std::vector<std::vector<RECT>>& drawInfo, Module* module, int i){

	rect->left = startX;
	rect->top = startY;
	rect->bottom = rect->top + size.cy + verticalMargin / 2;
	rect->right = rect->left + size.cx + horizontalMargin;

	RECT rectToDraw = *rect;

	drawInfo[sizeOfHierarchy - i - 1].push_back(*rect);

	SetDCBrushColor(hdc, cppFileHeaderColor);
	SetDCPenColor(hdc, RGB(0, 0, 0));

	Rectangle(hdc, rect->left - 1, rect->top - 1, rect->right + 1, rect->bottom + 1);
	FillRect(hdc, rect, (HBRUSH)GetStockObject(DC_BRUSH));
	SetBkMode(hdc, TRANSPARENT);
	DrawText(hdc, module->GetCppFile()->GetFilename(), strlen(module->GetCppFile()->GetFilename()), rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	if (module->GetCppFile()->GetDependencies().size() > 0) {

		int prevStartY = startY;

		tempDiff1 = abs(rect->bottom - rect->top);

		rect->left = startX;
		rect->top = startY + verticalMargin / 1.5 + 1;
		rect->bottom = rect->top + (verticalMargin / 2 * (module->GetCppFile()->GetDependencies().size() + 1));
		rect->right = rect->left + size.cx + horizontalMargin;

		tempDiff2 = abs(rect->bottom - rect->top);

		if (tempDiff1 + tempDiff2 > maxRectHeight)
			maxRectHeight = tempDiff1 + tempDiff2;

		SetDCBrushColor(hdc, cppFileContentColor);

		Rectangle(hdc, rect->left - 1, rect->top - 1, rect->right + 1, rect->bottom + 1);
		rectToDraw.bottom = rect->bottom + 1;
		FillRect(hdc, rect, (HBRUSH)GetStockObject(DC_BRUSH));

		for (auto it = module->GetCppFile()->GetDependencies().begin(); it != module->GetCppFile()->GetDependencies().end(); it++) {
			startY += verticalMargin / 2;

			rect->left = startX;
			rect->top = startY;
			rect->bottom = rect->top + size.cy + verticalMargin;
			rect->right = rect->left + size.cx + horizontalMargin;

			SetTextColor(hdc, RGB(35, 35, 255));
			DrawText(hdc, include, strlenInc, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

			rect->left += incSize.cx;

			SetTextColor(hdc, RGB(0, 0, 0));
			DrawText(hdc, (*it)->GetFilename(), strlen((*it)->GetFilename()), rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}
		
		startY = rect->bottom - 10;
		if (module->GetRcFile() != nullptr)
			module->GetDependencies().push_back(module->GetRcFile());

		for (auto it = module->GetDependencies().begin(); it != module->GetDependencies().end(); it++) {
			rect->left = startX;
			rect->top = startY;
			rect->bottom = rect->top + size.cy + verticalMargin / 2;
			rect->right = rect->left + size.cx + horizontalMargin;

			std::string fileNameStr = std::string((*it)->GetFilename());
		    char* ext = strrchr((*it)->GetFilename(), '.');


		
			if (!strcmp(ext, ".rc"))
				SetDCBrushColor(hdc, rcFileColor);
			else
				SetDCBrushColor(hdc, hFileHeaderColor);

			SetDCPenColor(hdc, RGB(0, 0, 255));

			HPEN hDashedPen = CreatePen(PS_DASH, 1, RGB(0, 0, 0));
			HPEN hSolidPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
			SelectObject(hdc, hDashedPen);

			MoveToEx(hdc, rect->left - 1, rect->top - 1, NULL);
			LineTo(hdc, rect->right + 1, rect->top - 1);

			if (it - module->GetDependencies().begin() == module->GetDependencies().size() - 1) {
				SelectObject(hdc, hSolidPen);
			}

			MoveToEx(hdc, rect->left - 1, rect->bottom + 1, NULL);
			LineTo(hdc, rect->right + 1, rect->bottom + 1);

			SelectObject(hdc, hSolidPen);

			MoveToEx(hdc, rect->left - 1, rect->top - 1, NULL);
			LineTo(hdc, rect->left - 1, rect->bottom + 1);

			MoveToEx(hdc, rect->right, rect->top - 1, NULL);
			LineTo(hdc, rect->right, rect->bottom + 1);

			
			FillRect(hdc, rect, (HBRUSH)GetStockObject(DC_BRUSH));
			SetBkMode(hdc, TRANSPARENT);
			DrawText(hdc, (*it)->GetFilename(), strlen((*it)->GetFilename()), rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

			auto deps = (*it)->GetDependencies();

			SetDCBrushColor(hdc, hFileContentColor);

			if (deps.size() > 0) {

				rect->left = startX;
				rect->top = rect->bottom;
				rect->bottom = rect->top + (verticalMargin / 2 * (deps.size() + 1));
				rect->right = rect->left + size.cx + horizontalMargin;

				Rectangle(hdc, rect->left - 1, rect->top - 1, rect->right + 1, rect->bottom + 1);
				FillRect(hdc, rect, (HBRUSH)GetStockObject(DC_BRUSH));
				
				for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
					startY += verticalMargin / 2;

					rect->left = startX;
					rect->top = startY;
					rect->bottom = rect->top + size.cy + verticalMargin;
					rect->right = rect->left + size.cx + horizontalMargin;

					SetTextColor(hdc, RGB(0,0,255));
					DrawText(hdc, include, strlenInc, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

					rect->left += incSize.cx;

					SetTextColor(hdc, RGB(0, 0, 0));
					DrawText(hdc, (*it1)->GetFilename(), strlen((*it1)->GetFilename()), rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
				}
			}
			startY = rect->bottom;
			rectToDraw.bottom = startY;
		}
		if (module->GetRcFile() != nullptr)
			module->GetDependencies().pop_back();
		startY = prevStartY;
	}

	return rectToDraw;
}

COLORREF Viewer::SelectColor(HWND hWnd) {
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = RGB(0, 0, 0);

	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc) == TRUE)
	{
		return cc.rgbResult;
	}

	return NULL;
}

void Viewer::SelectHFileHeaderColor(HWND hWnd) {
	hFileHeaderColor = SelectColor(hWnd);
	InvalidateRect(hWnd, NULL, TRUE);
	WriteColorsToFile("settings.txt");
}

void Viewer::SelectHFileContentColor(HWND hWnd) {
	hFileContentColor = SelectColor(hWnd);
	InvalidateRect(hWnd, NULL, TRUE);
	WriteColorsToFile("settings.txt");
}

void Viewer::SelectCppFileHeaderColor(HWND hWnd) {
	cppFileHeaderColor = SelectColor(hWnd);
	InvalidateRect(hWnd, NULL, TRUE);
	WriteColorsToFile("settings.txt");
}

void Viewer::SelectCppFileContentColor(HWND hWnd) {
	cppFileContentColor = SelectColor(hWnd);
	InvalidateRect(hWnd, NULL, TRUE);
	WriteColorsToFile("settings.txt");
}

void Viewer::SelectRcFileColor(HWND hWnd) {
	rcFileColor = SelectColor(hWnd);
	InvalidateRect(hWnd, NULL, TRUE);
	WriteColorsToFile("settings.txt");
}

void Viewer::WriteColorsToFile(char* filename) {
	std::ofstream ofs(filename);
	ofs << hFileHeaderColor << "\t" << hFileContentColor << "\t"
		<< cppFileHeaderColor << "\t" << cppFileContentColor << "\t" 
		<< rcFileColor << "\t";
	ofs.close();
}

bool Viewer::ReadColorsFromFile(char* filename){
	std::ifstream ifs(filename);
	if (!ifs.good())
		return false;
	ifs >> hFileHeaderColor >> hFileContentColor
		>> cppFileHeaderColor >> cppFileContentColor
		>> rcFileColor;
	ifs.close();
	return true;
}