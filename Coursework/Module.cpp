#include "Module.h"

Module::Module(File* cppFile, File* rcFile) {
	hFiles = cppFile->GetDependencies();
	this->cppFile = cppFile;
	this->rcFile = rcFile;
}

std::vector<File*> Module::GetHFiles() {
	return hFiles;
}


void Module::SetRcFile(File* file) {
	this->rcFile = file;
}

void Module::AddModuleDependancy(Module* module) {
	moduleDependencies.push_back(module);
}

std::vector<Module*>& Module::GetModuleDependencies() {
	return moduleDependencies;
}

void Module::InitDependencies() {
	dependencies.clear();

	bool search;

	for (auto it = hFiles.begin(); it != hFiles.end(); it++) {
		auto deps = (*it)->GetDependencies();
		for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
			search = false;
			for (auto it2 = dependencies.begin(); it2 != dependencies.end(); it2++) {
				auto innerDeps = (*it2)->GetDependencies();
				for (auto it3 = innerDeps.begin(); it3 != innerDeps.end(); it3++) {
					if (*it3 == *it1) {
						search = true;
						break;
					}
				}
				if (search)
					break;
			}
			if (!search)
				dependencies.push_back(*it1);
		}
	}

	for (auto it = hFiles.begin(); it != hFiles.end(); it++) {
		dependencies.push_back(*it);
	}

	UniqueVectorVals(dependencies);
}

void Module::UniqueVectorVals(std::vector<File*>& vec) {
	std::vector<File*> uniqueDeps;

	for (int i = 0; i < vec.size(); i++) {
		bool search = false;

		for (int j = 0; j < uniqueDeps.size(); j++) {
			if (uniqueDeps[j] == vec[i]) {
				search = true;
				break;
			}
		}

		if (!search) {
			uniqueDeps.push_back(vec[i]);
		}
	}

	vec = uniqueDeps;
}

std::vector<File*>& Module::GetDependencies() {
	return dependencies;
}

bool Module::InnerDepsContainFile(File* file) {
	for (auto it = hFiles.begin(); it != hFiles.end(); it++) {
		auto deps = (*it)->GetDependencies();

		for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
			if (*it1 == file)
				return true;
		}
	}
	return false;
}

bool Module::CheckSubmodulesHaveFile(File* file) {

	for (auto it = moduleDependencies.begin(); it != moduleDependencies.end(); it++) {
		auto deps = (*it)->GetDependencies();
		bool found = false;
		for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
			if ((*it1) == file)
				found = true;
		}
		if (found)
			return true;
		else {
			if (((*it)->CheckSubmodulesHaveFile(file))) {
				return true;
			}
		}
	}
	return false;
}

bool Module::ContainsDependency(File* file) {
	for (auto it = hFiles.begin(); it != hFiles.end(); it++) {
		auto deps = (*it)->GetDependencies();

		for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
			if (*it1 == file)
				return true;
		}
	}
	return false;
}

bool Module::ContainsDependency(Module* module) {
	for (auto it = moduleDependencies.begin(); it != moduleDependencies.end(); it++) {
		if ((*it)->GetCppFile() == module->GetCppFile())
			return true;
		else {
			bool res = ((*it)->ContainsDependency(module));
			if (res)
				return true;
		}
	}
	return false;
}

void Module::DeleteModuleDependency(int index) {
	moduleDependencies.erase(moduleDependencies.begin() + index);
}

void Module::DeleteModuleFileDependency(int index) {
	dependencies.erase(dependencies.begin() + index);
}

int Module::GetHierarchyLevel() {
	return this->hierarchyLevel;
}

void Module::SetHierarchyLevel(int level) {
	this->hierarchyLevel = level;
}

RECT& Module::GetCoords() {
	return coords;
}

void Module::SetCoords(RECT coords) {
	this->coords = coords;
}

File* Module::GetCppFile() {
	return cppFile;
}

File* Module::GetRcFile() {
	return rcFile;
}