#pragma once

#include "File.h"
#include "Module.h"

class Hierarchy {
	char* selectedPath;

	std::vector<File*> hFiles;
	std::vector<File*> cppFiles;
	std::vector<File*> rcFiles;

	std::vector<Module*> modules;
	std::vector<std::vector<Module*>> moduleHierarchy;
	
	int maxFilesInLine;
	int maxFilenameLength;
	
	std::vector<File*> FindFilesWithExtInFolder(char* mask);
	void InitDependecies(std::vector<File*>& files);
	File* GetHFileByName(char* filename);

	void FixHFilesInModules();

	int ExistsInPrevLevelDeps(Module* module, int levelStart, int levelEnd);

	bool FindModuleWithTheNameOfHFile(Module* module, File* file);
	bool FindHFileInPrevModules(Module* module, File* file, int level);

	void DeleteDuplicates(std::vector<File*>& vec1, std::vector<File*>& vec2);
	void DeleteDuplicates(std::vector<Module*>& vec1, std::vector<Module*>& vec2);

	File* GetFileHModule(char* fileName);
	const char* GetFileExtension(char* filename);
	int CompareFileNames(char* filename1, char* filename2);
	
	void InitModuleDependencies();
	void UniqueVectorVals(std::vector<int>& vec);
public:
	Hierarchy();
	Hierarchy(char* selectedPath);

	void InitFiles();
	void InitHierarchy();
	char* GetText();
	
	std::vector<std::vector<Module*>> GetHierarchy();
	
	int GetMaxFilesInLine();
	void SetMaxFilesInLine(int maxFilesInLine);

	int GetMaxFilenameLength();
	void SetMaxFilenameLength(int max);

	~Hierarchy();
};