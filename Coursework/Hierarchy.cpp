#include "stdafx.h"
#include "Hierarchy.h"
#include <Windows.h>
#include <ShlObj.h>
#include <fstream>
#include <string>
#include <algorithm>

Hierarchy::Hierarchy() {
	this->selectedPath = nullptr;
	this->maxFilenameLength = 0;
	this->maxFilesInLine = 0;
}

Hierarchy::Hierarchy(char* selectedPath) {
	this->selectedPath = new char[strlen(selectedPath) + 1];
	strcpy(this->selectedPath, selectedPath);

	this->maxFilenameLength = 0;
	this->maxFilesInLine = 0;
}

void Hierarchy::InitFiles() {
	hFiles = FindFilesWithExtInFolder("\\*.h");
	cppFiles = FindFilesWithExtInFolder("\\*.cpp");
	rcFiles = FindFilesWithExtInFolder("\\*.rc");

	InitDependecies(hFiles);
	InitDependecies(cppFiles);
	InitDependecies(rcFiles);
}

std::vector<File*> Hierarchy::FindFilesWithExtInFolder(char* mask) {
	std::vector<File*> foundFiles;
	
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA findFiles;

	char* tempSelectedPath = new char[strlen(selectedPath) + strlen(mask) + 2];
	strcpy(tempSelectedPath, selectedPath);
	strcat(tempSelectedPath, mask);

	hFind = FindFirstFile(tempSelectedPath, &findFiles);
	if (hFind == INVALID_HANDLE_VALUE)
		return foundFiles;

	do {
		if (!(findFiles.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			int len = strlen(findFiles.cFileName);

			if (maxFilenameLength < len && len < sizeof(findFiles.cFileName))
				maxFilenameLength = len;

			foundFiles.push_back(new File(findFiles.cFileName));
		}
	} while (FindNextFile(hFind, &findFiles) != 0);

	delete tempSelectedPath;

	return foundFiles;
}

File* Hierarchy::GetHFileByName(char* filename) {
	for (auto it = hFiles.begin(); it != hFiles.end(); it++) {
		if (CompareFileNames((*it)->GetFilename(), filename) == 0)
			return (*it);
	}
}

void Hierarchy::InitDependecies(std::vector<File*>& files) {
	for (auto it = files.begin(); it != files.end(); it++) {
		std::string fullPath;
		fullPath += selectedPath;
		fullPath += "\\";
		fullPath += (*it)->GetFilename();
		
		std::ifstream ifs(fullPath);
		std::string line;

		size_t first = 0;

		while (std::getline(ifs, line)) {
			if (line.substr(0, 8).compare("#include") == 0 &&
				(first = line.find("\"", 0)) != std::string::npos) {
				size_t last = line.find('"\"', first + 1);

				std::string includedFile = line.substr(first + 1, last - first - 1);
				File* dep = GetHFileByName(const_cast<char*>(includedFile.c_str()));
				if (dep != nullptr)
					(*it)->AddDependency(dep);
			}
		}
	}
}

int Hierarchy::ExistsInPrevLevelDeps(Module* module, int levelStart, int levelEnd) {
	for (int i = levelStart - 1; i >= levelEnd; i--) {
		for (auto it = moduleHierarchy[i].begin(); it != moduleHierarchy[i].end(); it++) {
			if (strcmp((*it)->GetCppFile()->GetFilename(), module->GetCppFile()->GetFilename()) == 0) {
				return i;
			}
		}
	}
	return -1;
}

void Hierarchy::DeleteDuplicates(std::vector<File*>& vec1, std::vector<File*>& vec2) {
	for (int i = 0; i < vec1.size(); i++) {
		for (int j = 0; j < vec2.size(); j++) {
			if (strcmp(vec1[i]->GetFilename(), vec2[j]->GetFilename()) == 0) {
				vec2.erase(vec2.begin() + j);
				break;
			}
		}
	}
}

void Hierarchy::DeleteDuplicates(std::vector<Module*>& vec1, std::vector<Module*>& vec2) {
	for (int i = 0; i < vec1.size(); i++) {
		for (int j = 0; j < vec2.size(); j++) {
			if (strcmp(vec1[i]->GetCppFile()->GetFilename(), vec2[j]->GetCppFile()->GetFilename()) == 0) {
				vec2.erase(vec2.begin() + j);
				break;
			}
		}
	}
}

void Hierarchy::InitHierarchy() {
	InitModuleDependencies();
	moduleHierarchy.push_back(std::vector<Module*>());
	std::vector<Module*> tempModules = modules;
	int tempFilesSize = tempModules.size();
	for (auto it = tempModules.begin(); it != tempModules.end(); it++) {
		if ((*it)->GetModuleDependencies().size() == 0) {
			moduleHierarchy[0].push_back((*it));
			(*it)->SetHierarchyLevel(0);
		}
	}
	
	maxFilesInLine = moduleHierarchy[0].size();
	DeleteDuplicates(moduleHierarchy[0], tempModules);

	int level = 1;
	while (tempModules.size() != 0) {
		moduleHierarchy.push_back(std::vector<Module*>());
		for (auto it = tempModules.begin(); it != tempModules.end(); it++) {
			auto deps = (*it)->GetModuleDependencies();
			
			int depLevel;
			for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
				depLevel = ExistsInPrevLevelDeps((*it1), moduleHierarchy.size() - 1, 0);

				if (depLevel == -1)
					break;
			}

			if (depLevel != -1) {
				moduleHierarchy[level].push_back(*it);
			}
		}
		DeleteDuplicates(moduleHierarchy[level], tempModules);

		if (moduleHierarchy[level].size() > maxFilesInLine)
			maxFilesInLine = moduleHierarchy[level].size();

		for (auto it = moduleHierarchy[level].begin(); it != moduleHierarchy[level].end(); it++) {
			(*it)->SetHierarchyLevel(level);
		}

		level++;
	}

	FixHFilesInModules();
}

void Hierarchy::FixHFilesInModules() {
	for (int i = moduleHierarchy.size() - 1; i >= 0; i--) {
		for (auto it = moduleHierarchy[i].begin(); it != moduleHierarchy[i].end(); it++) {
			auto deps = (*it)->GetDependencies();

			std::vector<int> toDelete;
			for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
				if (FindHFileInPrevModules(*it, *it1, it1 - deps.begin())) {
					toDelete.push_back(it1 - deps.begin());
				}
			}

			for (int j = toDelete.size() - 1; j >= 0; j-- ) {
				(*it)->DeleteModuleFileDependency(toDelete[j]);
			}
		}
	}
}

bool Hierarchy::FindModuleWithTheNameOfHFile(Module* module, File* file) {

	if (CompareFileNames(module->GetCppFile()->GetFilename(), file->GetFilename())) {
		auto it = modules.begin();
		for (; it != modules.end(); it++) {
			if (!CompareFileNames((*it)->GetCppFile()->GetFilename(), file->GetFilename()))
			{
				return false;
			}
		}
	}
	return false;
}

bool Hierarchy::FindHFileInPrevModules(Module* module, File* file, int level) {
	if (CompareFileNames(module->GetCppFile()->GetFilename(), file->GetFilename())) {
		return module->CheckSubmodulesHaveFile(file);
	}
	
	return false;
}

void Hierarchy::InitModuleDependencies() {

	modules.clear();
	for (auto it = cppFiles.begin(); it != cppFiles.end(); it++) {
		Module* m = new Module(*it, NULL);
		m->InitDependencies();
		modules.push_back(m);
	}

	for (auto it = modules.begin(); it != modules.end(); it++) {
		auto deps = (*it)->GetDependencies();
		for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
			for (auto it2 = modules.begin(); it2 != modules.end(); it2++) {
				if (*it2 != *it) {
					if (!CompareFileNames((*it2)->GetCppFile()->GetFilename(), (*it1)->GetFilename())) {
						(*it)->AddModuleDependancy(*it2);
					}
				}
			}
		}
	}

	for (auto it = modules.begin(); it != modules.end(); it++) {
		auto deps = (*it)->GetModuleDependencies();

		std::vector<int> toDelete;

		for (auto it1 = deps.begin(); it1 != deps.end(); it1++) {
			for (auto it2 = deps.begin(); it2 != deps.end(); it2++) {
				if (*it1 != *it2) {
					if ((*it1)->ContainsDependency(*it2)) {
						toDelete.push_back(it2 - deps.begin());					
					}
				}
			}
		}
		std::sort(toDelete.begin(), toDelete.end());
		UniqueVectorVals(toDelete);

		for (int i = toDelete.size() - 1; i >= 0; i--) {
			(*it)->DeleteModuleDependency(toDelete[i]);
		}
	}

	for (auto it1 = modules.begin(); it1 != modules.end(); it1++) {
		for (auto it = rcFiles.begin(); it != rcFiles.end(); it++) {
			if ((*it) != nullptr) {
				if (!CompareFileNames((*it)->GetFilename(), (*it1)->GetCppFile()->GetFilename()))
					(*it1)->SetRcFile(*it);
			}
		}
	}
}

void Hierarchy::UniqueVectorVals(std::vector<int>& vec) {
	std::vector<int> uniqueDeps;

	for (int i = 0; i < vec.size(); i++) {
		bool search = false;

		for (int j = 0; j < uniqueDeps.size(); j++) {
			if (uniqueDeps[j] == vec[i]) {
				search = true;
				break;
			}
		}

		if (!search) {
			uniqueDeps.push_back(vec[i]);
		}
	}

	vec = uniqueDeps;
}


std::vector<std::vector<Module*>> Hierarchy::GetHierarchy() {
	return moduleHierarchy;
}

Hierarchy::~Hierarchy() {
	if (selectedPath != nullptr)
		delete selectedPath;

	selectedPath = nullptr;
}

char* Hierarchy::GetText() {
	std::string str;

	int counter = 0;

	for (auto it = modules.begin(); it != modules.end(); it++, counter++) {
		str += (*it)->GetCppFile()->GetFilename();

		auto inner = (*it)->GetDependencies();
		str += "(";
		for (auto it1 = inner.begin(); it1 != inner.end(); it1++) {
			str += (*it1)->GetFilename();
			str += " ";
		}
		str += ")";

		str += "\n";
		
	}
	counter = 0;
	for (auto it = moduleHierarchy.begin(); it != moduleHierarchy.end(); it++, counter++) {
		auto mods = (*it);
		char* buff = new char[7];
		_itoa(counter, buff, 10);
		str += buff;
		str += "=";
		str += "(";
		for (auto it1 = mods.begin(); it1 != mods.end(); it1++) {
			str += (*it1)->GetCppFile()->GetFilename();
			str += " ";
		}
		str += ")\n";
	}

	char* res = new char[str.length() + 1];
	strcpy(res, str.c_str());

	return res;
}

File* Hierarchy::GetFileHModule(char* fileName) {
	std::string fileNameStr = fileName, hFileStr;
	fileNameStr = fileNameStr.substr(0, fileNameStr.find_last_of('.')).c_str();
	for (auto it = hFiles.begin(); it != hFiles.end(); it++) {
		hFileStr = (*it)->GetFilename();
		if (strcmp(hFileStr.substr(0, hFileStr.find_last_of('.')).c_str(), fileNameStr.c_str()) == 0)
			return *it;
	}

	return nullptr;

}

const char* Hierarchy::GetFileExtension(char* filename) {
	std::string fileNameStr = std::string(filename);
	return fileNameStr.substr(fileNameStr.find_last_of('.')).c_str();
}

int Hierarchy::CompareFileNames(char* filename1, char* filename2) {
	std::string filenameStr1 = filename1, filenameStr2 = filename2;
	filenameStr1 = filenameStr1.substr(0, filenameStr1.find_last_of('.')).c_str();
	filenameStr2 = filenameStr2.substr(0, filenameStr2.find_last_of('.')).c_str();

	return filenameStr1.compare(filenameStr2);
}

int Hierarchy::GetMaxFilesInLine() {
	return this->maxFilesInLine;
}

void Hierarchy::SetMaxFilesInLine(int maxFilesInLine) {
	this->maxFilesInLine = maxFilesInLine;
}

int Hierarchy::GetMaxFilenameLength() {
	return this->maxFilenameLength;
}

void Hierarchy::SetMaxFilenameLength(int max) {
	this->maxFilenameLength = max;
}