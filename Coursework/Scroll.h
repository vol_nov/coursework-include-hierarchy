#pragma once

#include <Windows.h>

struct ScrollInfo {
	int scrollCounterX;
	int scrollCounterY;
	int scrollDx;
	int scrollDy;

	ScrollInfo();
	void InitScrollInfo(HWND hWnd, int maxWidth, int maxHeight, RECT rectClient);
};

void OnHScroll(HWND hWnd, WPARAM wParam, LPARAM lParam, ScrollInfo* scrollInfo, int maxWidth);
void OnVScroll(HWND hWnd, WPARAM wParam, LPARAM lParam, ScrollInfo* scrollInfo, int maxHeight);