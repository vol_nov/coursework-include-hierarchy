//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Coursework.rc
//
#define IDC_MYICON                      2
#define IDD_COURSEWORK_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_COURSEWORK                  107
#define IDI_SMALL                       108
#define IDC_COURSEWORK                  109
#define IDM_OPEN                        110
#define IDC_VSCROLLBAR                  111
#define IDC_HSCROLLBAR                  112
#define IDR_MAINFRAME                   128
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_BACK_H_FILE_HEADER           32776
#define ID_BACK_H_FILE_CONTENT          32777
#define ID_BACK_CPP_FILE_HEADER			32778
#define ID_BACK_CPP_FILE_CONTENT		32779
#define ID_BACK_RC_FILE					32780
#define IDC_STATIC						32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
