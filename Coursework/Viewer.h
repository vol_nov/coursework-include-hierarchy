#pragma once
#include "Hierarchy.h"
#include "Scroll.h"

class Viewer {
private:
    HFONT hFont;

	SIZE size, incSize;
	int startX, startY;
	int horizontalMargin, verticalMargin;
	int spaceBetweenFilesHor, spaceBetweenFilesVer;

	int maxWidth;
	int maxHeight;

	int outCrossLineDx;
	int outCrossLineDy;

	void OrderVectorByLeftCoords(std::vector<Module*>& deps);

	void InitConnectionLineInfo(RECT* coords, Module* file, Module* dep, int endXdX);
	void SetOrdinaryConnectionCoords(std::vector<POINT>& pArr, RECT coords, int drawDy);
	void SetCrossConnectionCoords(std::vector<POINT>& pArr, RECT coords, ScrollInfo* scrollInfo, Module* dep);
	void DrawArrow(HDC hdc, int endX, int endY);

	RECT DrawFile(HDC hdc, LPRECT rect, int sizeOfHierarchy, std::vector<std::vector<RECT>>& drawInfo, Module* module, int i);

	char* include = " #include ";
	int strlenInc = strlen(include);

	int	maxRectHeight = spaceBetweenFilesVer;
	int tempDiff1, tempDiff2;

	COLORREF hFileHeaderColor = RGB(28, 232, 255), hFileContentColor = RGB(220, 220, 220);
	COLORREF cppFileHeaderColor = RGB(33, 255, 169), cppFileContentColor = RGB(243,243,243);
	COLORREF rcFileColor = RGB(0,255,255);

	CHOOSECOLOR cc;
	COLORREF acrCustClr[16];

	COLORREF SelectColor(HWND);
	void WriteColorsToFile(char* filename);
	bool ReadColorsFromFile(char* filename);
public:
	Viewer(int maxFilesInLine, int hierarchySize);
	void OnPaint(HWND hWnd, Hierarchy* hierarchy, ScrollInfo* scrollInfo, RECT rectClient);
	
	int GetMaxWidth();
	int GetMaxHeight();

	void SelectHFileHeaderColor(HWND);
	void SelectHFileContentColor(HWND);
	void SelectCppFileHeaderColor(HWND);
	void SelectCppFileContentColor(HWND);
	void SelectRcFileColor(HWND);

	void SetMaxFileStrSize(HDC hdc, Hierarchy* hierarchy);
	void SetMaxWidthAndHeight(int maxFilesInLine, int hierarchySize);
};

