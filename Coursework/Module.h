#pragma once

#include "File.h"

class Module {
	std::vector<File*> hFiles;
	File* cppFile;
	File* rcFile;
	std::vector<Module*> moduleDependencies;
	std::vector<File*> dependencies;
	int hierarchyLevel;
	RECT coords;
public:
	Module(File* cppFile, File* rcFile);
	
	void AddModuleDependancy(Module* module);
	std::vector<Module*>& GetModuleDependencies();
	std::vector<File*>& GetDependencies();
	void UniqueVectorVals(std::vector<File*>& vec);
	void InitDependencies();

	std::vector<File*> GetHFiles();
	File* GetCppFile();
	File* GetRcFile();

	void SetRcFile(File* file);
	
	bool InnerDepsContainFile(File* file);
	
	bool ContainsDependency(File* file);
	bool ContainsDependency(Module* module);

	void DeleteModuleDependency(int index);
	void DeleteModuleFileDependency(int index);

	bool CheckSubmodulesHaveFile(File* file);

	int GetHierarchyLevel();
	void SetHierarchyLevel(int level);

	RECT& GetCoords();
	void SetCoords(RECT coords);
};