#include "stdafx.h"
#include <commdlg.h>
#include <ShlObj.h>
#include <vector>

#include "Resource.h"
#include "Viewer.h"

#define MAX_LOADSTRING 100

HINSTANCE hInst;                               
CHAR szTitle[MAX_LOADSTRING];                 
CHAR szWindowClass[MAX_LOADSTRING];           

ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

char* selected = nullptr;
char selectedPath[255];

HWND hWndHScrollBar;
HWND hWndVScrollBar;
RECT currentScroll;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadString(hInstance, IDC_COURSEWORK, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_COURSEWORK));

    MSG msg;

    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_COURSEWORK));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCE(IDC_COURSEWORK);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; 

   HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL | ES_AUTOHSCROLL,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
   
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED)
	{
		if (selected != nullptr)
			delete selected;

		selected = new char[strlen((char*)lpData) + 1];
		strcpy(selected, ((char*)lpData));

		SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
	}

	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	/*static OPENFILENAME ofn;*/
	static BROWSEINFO brInfo;
	static Hierarchy* hierarchy;
	static Viewer* viewer;
	static ScrollInfo* scrollInfo;

	static RECT rectClient;
	
    switch (message)
    {
	case WM_CREATE:

		brInfo.hwndOwner = hWnd;
		brInfo.pszDisplayName = selected;
		brInfo.lpszTitle = "������� ��������� ��� ������� ������";
		
		//C:\\Users\\vovan\\Documents\\Visual Studio 2015\\Projects\\OOPLab4\\OOPLab4
		//C:\\Users\\vovan\\Desktop\\TestCourseWork
		//C:\\Users\\vovan\\Desktop\\Coursework\\Coursework
		/*hierarchy = new Hierarchy();
		hierarchy->InitFiles();
		hierarchy->InitHierarchy();

		viewer = new Viewer(hierarchy->GetMaxFilenameLength(), hierarchy->GetMaxFilesInLine());*/
		scrollInfo = new ScrollInfo();

		GetClientRect(hWnd, &rectClient);

		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            switch (wmId)
            {
			case IDM_OPEN:
			{
				LPITEMIDLIST iil = SHBrowseForFolder(&brInfo);
				if (iil != nullptr) {
					SHGetPathFromIDList(iil, selectedPath);
					
					hierarchy = new Hierarchy(selectedPath);
					hierarchy->InitFiles();
					hierarchy->InitHierarchy();

					viewer = new Viewer(hierarchy->GetMaxFilesInLine(), hierarchy->GetMaxFilesInLine());

					InvalidateRect(hWnd, NULL, TRUE);
				}
			}
			break;
			case ID_BACK_H_FILE_HEADER:
				viewer->SelectHFileHeaderColor(hWnd);
				break;
			case ID_BACK_H_FILE_CONTENT:
				viewer->SelectHFileContentColor(hWnd);
				break;
			case ID_BACK_CPP_FILE_HEADER:
				viewer->SelectCppFileHeaderColor(hWnd);
				break;
			case ID_BACK_CPP_FILE_CONTENT:
				viewer->SelectCppFileContentColor(hWnd);
				break;
			case ID_BACK_RC_FILE:
				viewer->SelectRcFileColor(hWnd);
				break;
			case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;

	case WM_PAINT:
		viewer->OnPaint(hWnd, hierarchy, scrollInfo, rectClient);
		break;
	case WM_VSCROLL: 
		OnVScroll(hWnd, wParam, lParam, scrollInfo, viewer->GetMaxHeight());
		break;
	case WM_HSCROLL: 
		OnHScroll(hWnd, wParam, lParam, scrollInfo, viewer->GetMaxWidth());
		break;
    case WM_DESTROY:
		delete hierarchy;
		delete viewer;
		delete scrollInfo;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

