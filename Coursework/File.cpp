#include "stdafx.h"
#include "File.h"
#include <algorithm>

File::File() {
	filename = nullptr;
}

File::File(char* filename) {
	this->filename = new char[strlen(filename) + 1];
	strcpy(this->filename, filename);
}

File::File(char* filename, std::vector<File*> dependencies) :File(filename) {
	this->dependencies = dependencies;
}

void File::AddDependency(File* file) {
	dependencies.push_back(file);
}

char* File::GetFilename() {
	return filename;
}

std::vector<File*>& File::GetDependencies() {
	return dependencies;
}

bool File::ContainsDependency(File* file) {
	for (auto it = dependencies.begin(); it != dependencies.end(); it++) {
		if (!strcmp((*it)->GetFilename(), file->GetFilename()))
			return true;
		else
		{
			bool res = ((*it)->ContainsDependency(file));
			if (res)
				return true;
		}
	}
	return false;
}

File::~File() {
	if (filename != nullptr)
		delete filename;

	filename = nullptr;
}
